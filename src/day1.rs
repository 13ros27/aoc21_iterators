fn count_increases(data: impl Iterator<Item = usize>) -> usize {
    data.collect::<Vec<usize>>()
        .windows(2)
        .filter(|w| w[1] > w[0])
        .count()
}

pub fn day1a(input: &str) -> usize {
    count_increases(
        input
            .lines()
            .map(|n| n.parse().expect("Failed parsing input string to usize")),
    )
}

pub fn day1b(input: &str) -> usize {
    count_increases(
        input
            .lines()
            .map(|n| n.parse().expect("Failed parsing input string to usize"))
            .collect::<Vec<usize>>()
            .windows(3)
            .map(|w| w.iter().sum()),
    )
}
