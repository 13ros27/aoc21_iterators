#![deny(clippy::all)]
#![allow(dead_code)]
mod day1;
mod day2;
mod day3;
mod day4;
mod utils;

fn main() {
    let sol = day4::day4a(&read_file("day4"));
    println!("{:?}", sol.0 * sol.1);
}

fn read_file(name: &str) -> String {
    std::fs::read_to_string(format!("inputs/{name}.txt")).expect("Failed reading string from file")
}
