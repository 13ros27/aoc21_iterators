use crate::utils::*;

pub fn day3a(input: &str) -> (usize, usize) {
    input
        .lines()
        .fold(
            vec![Vec::new(); input.lines().next().unwrap().len()],
            |mut acc, l| {
                l.chars().enumerate().for_each(|(i, c)| acc[i].push(c));
                acc
            },
        )
        .iter()
        .map(|c| {
            c.iter().fold((0, 0), |(z, o), e| match e {
                '0' => (z + 1, o),
                '1' => (z, o + 1),
                _ => panic!(),
            })
        })
        .map(|(z, o)| ((o > z) as usize, (z > o) as usize))
        .rev()
        .scan(1, |acc, (m, l)| {
            let ret = (m * *acc, l * *acc);
            *acc *= 2;
            Some(ret)
        })
        .sum_pairs()
}

fn determine_rating(input: &str, check: fn((usize, usize)) -> usize) -> usize {
    (0..input.lines().next().unwrap().len())
        .scan(input.lines().collect::<Vec<_>>(), |lines, i| {
            *lines = lines
                .iter()
                .filter(|l| {
                    l.chars().nth(i).unwrap().to_digit(10)
                        == Some(check(
                            lines
                                .iter()
                                .map(|l| match l.chars().nth(i).unwrap() {
                                    '0' => (1, 0),
                                    '1' => (0, 1),
                                    _ => panic!(),
                                })
                                .sum_pairs(),
                        ) as u32)
                })
                .map(|l| l.to_owned())
                .collect::<Vec<_>>();
            lines.first().copied()
        })
        .last()
        .unwrap()
        .chars()
        .rev()
        .scan(1, |acc, b| {
            let ret = b.to_digit(10).unwrap() * *acc;
            *acc *= 2;
            Some(ret)
        })
        .sum::<u32>() as usize
}

pub fn day3b(input: &str) -> (usize, usize) {
    (
        determine_rating(input, |(a, b)| (b >= a) as usize),
        determine_rating(input, |(a, b)| (b < a) as usize),
    )
}
