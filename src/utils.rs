use std::ops::Add;

pub trait IteratorExt<A, B> {
    fn sum_pairs(self) -> (A, B);
}

impl<I, A: Add<Output = A>, B: Add<Output = B>> IteratorExt<A, B> for I
where
    I: Iterator<Item = (A, B)>,
{
    fn sum_pairs(self) -> (A, B) {
        self.reduce(|(m0, l0), (m1, l1)| (m0 + m1, l0 + l1))
            .unwrap()
    }
}
