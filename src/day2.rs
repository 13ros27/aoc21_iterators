use crate::utils::*;

pub fn day2a(input: &str) -> (isize, isize) {
    input
        .lines()
        .map(|l| l.split_whitespace().collect::<Vec<_>>())
        .map(|g| {
            (
                match g[0] {
                    "forward" => (1, 0),
                    "down" => (0, 1),
                    "up" => (0, -1),
                    _ => panic!("{} is not a direction", g[0]),
                },
                g[1].parse::<isize>().unwrap(),
            )
        })
        .map(|((h, d), b)| (h * b, d * b))
        .sum_pairs()
}

pub fn day2b(input: &str) -> (isize, isize) {
    input
        .lines()
        .map(|l| l.split_whitespace().collect::<Vec<_>>())
        .map(|g| {
            (
                match g[0] {
                    "forward" => (0, 1, 1),
                    "down" => (1, 0, 0),
                    "up" => (-1, 0, 0),
                    _ => panic!("{} is not a direction", g[0]),
                },
                g[1].parse::<isize>().unwrap(),
            )
        })
        .scan(0, |aim, ((a, h, d), x)| {
            *aim += a * x;
            Some((h * x, d * x * *aim))
        })
        .sum_pairs()
}
