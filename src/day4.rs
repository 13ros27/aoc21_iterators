pub fn day4a(input: &str) -> (usize, usize) {
    let mut sol = 0;
    (
        input
            .lines()
            .next()
            .unwrap()
            .split(',')
            .map(|n| n.parse::<usize>().unwrap())
            .scan(
                input
                    .split("\r\n\r\n")
                    .skip(1)
                    .map(|t| {
                        t.lines()
                            .map(|l| {
                                l.split_whitespace()
                                    .map(|n| n.parse::<usize>().unwrap())
                                    .collect::<Vec<_>>()
                            })
                            .fold([Vec::new(), Vec::new()], |mut acc, l| {
                                (0..l.len()).for_each(|i| {
                                    if acc[1].get(i).is_none() {
                                        acc[1].push(Vec::new());
                                    }
                                    acc[1][i].push(l[i]);
                                });
                                acc[0].push(l);
                                acc
                            })
                            .into_iter()
                            .flatten()
                            .collect()
                    })
                    .collect::<Vec<Vec<_>>>(),
                |acc, n| {
                    if (0..acc.len()).fold(false, |acc2, i| {
                        if acc[i].iter().any(|s| s.is_empty()) {
                            sol = acc[i].iter().flatten().sum::<usize>() / 2;
                            true
                        } else {
                            acc[i] = acc[i]
                                .iter()
                                .map(|s| s.iter().filter(|&v| *v != n).copied().collect())
                                .collect();
                            acc2
                        }
                    }) {
                        None
                    } else {
                        Some(n)
                    }
                },
            )
            .last()
            .unwrap(),
        sol,
    )
}
